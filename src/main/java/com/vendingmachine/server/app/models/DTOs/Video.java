package com.vendingmachine.server.app.models.DTOs;

public class Video {

    private String fileName;
    private String uploadedName;
    private String companyName;
    private String description;

    public Video(String uploadedName, String companyName, String description) {
        this.uploadedName = uploadedName;
        this.companyName = companyName;
        this.description = description;
        setFileName();
    }

    public String getFileName() {
        return fileName;
    }

    private void setFileName() {
        fileName = (companyName + "-" + uploadedName+ "-" + System.currentTimeMillis()).replaceAll("\\s+","");
    }

    public String getUploadedName() {
        return uploadedName;
    }

    private void setUploadedName(String uploadedName) {
        this.uploadedName = uploadedName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
        setFileName();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
