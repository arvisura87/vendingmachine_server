package com.vendingmachine.server.app.VideoStorage;

public class StorageProperties {
    /**
     * Folder location for storing files
     */

    private String location = "C:\\Repos\\vendingmachine_server\\src\\main\\resources\\videos";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
