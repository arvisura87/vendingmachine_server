package com.vendingmachine.server.app.controllers.rest;

import com.vendingmachine.server.app.models.DTOs.Video;
import com.vendingmachine.server.app.VideoStorage.StorageFileNotFoundException;
import com.vendingmachine.server.app.VideoStorage.VideoStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class VideoRestController {

    VideoStorageService videoStorageService;

    @Autowired
    public VideoRestController(VideoStorageService videoStorageService) {
        this.videoStorageService = videoStorageService;
    }

    @RequestMapping(value = "/api/video/{video_id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> dowloadVideo(@PathVariable String video_id) {
        Resource file = videoStorageService.loadAsResource(video_id + ".mp4");

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @RequestMapping(value = "/api/video", method = RequestMethod.POST)
    public ResponseEntity uploadVideo(@RequestParam(name = "uploadedName") String uploadedName,
                                      @RequestParam(name = "companyName") String companyName,
                                      @RequestParam(name = "description") String description,
                                      @RequestParam(name = "videoFile") MultipartFile videoFile){
        videoStorageService.store(videoFile);
        Video newVideo = new Video(uploadedName, companyName, description);

        return ResponseEntity.ok(newVideo.getFileName());
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    //Test endpoint
    //***
    //***
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity test() {

        System.out.println("sout");
        return ResponseEntity.ok("test");
    }
}
